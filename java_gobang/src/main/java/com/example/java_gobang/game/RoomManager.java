package com.example.java_gobang.game;

import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

// 房间管理器类
// 这个类也希望有唯一实例
@Component
public class RoomManager {
    private ConcurrentHashMap<String, Room> rooms = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Integer, String > userIdRoomId = new ConcurrentHashMap<>();

    public void  add (Room room, int userId1, int userId2) {
        rooms.put(room.getRoomId(), room);
        userIdRoomId.put(userId1,room.getRoomId());
        userIdRoomId.put(userId2, room.getRoomId());



    }

    public void remove (String roomId, int userId1, int userId2) {
        rooms.remove(roomId);
        userIdRoomId.remove(userId1);
        userIdRoomId.remove(userId2);

    }

    public Room getRoomById (String roomId) {
      return rooms.get(roomId);
    }

    public Room  getRoomByUserId (int userId) {
        String roomId = userIdRoomId.get(userId);
        if (roomId == null) {
            // userID -> roomId 映射关系不存在 直接返回 null
            return null;
        }
        return rooms.get(roomId);

    }
}
